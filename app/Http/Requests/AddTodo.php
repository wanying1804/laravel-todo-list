<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class AddTodo extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'task' => 'string|required',
        ];
    }
}
