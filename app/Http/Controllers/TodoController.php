<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddTodo;
use App\Services\ReportService;
use Illuminate\Support\Facades\Auth;

/**
 * Class TodoController
 * @package App\Http\Controllers
 */
class TodoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getTodos()
    {
        return response()->json(Auth::user()->todos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeTodo(AddTodo $request)
    {
        $user = Auth::user();
        $user->todos()->firstOrCreate([
            'task' => $request->task
        ]);
        return response()->json($user->todos);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteTodo(int $id)
    {
        $user = Auth::user();
        $user->todos()->find($id)->delete($id);

        return response()->json($user->todos);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function markTodoAsDone(int $id)
    {
        $user = Auth::user();
        $user->todos()->find($id)->markDone();

        return response()->json($user->todos);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function markTodoAsUndone(int $id)
    {
        $user = Auth::user();
        $user->todos()->find($id)->markUndone();

        return response()->json($user->todos);
    }

    public function getReport()
    {
        $user = Auth::user();
        $report = app(ReportService::class)->generateTodoReportForUser($user);

        return response()->json($report);
    }
}
