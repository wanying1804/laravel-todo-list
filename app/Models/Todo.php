<?php

namespace App\Models;

use App\Observers\TodoObserver;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Todo
 * @package App
 */
class Todo extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'task',
        'finished_at',
        'deleted_at',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'finished_at',
    ];

    public static function boot()
    {
        parent::boot();

        static::observe(TodoObserver::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeUndones($query)
    {
        return $query->whereNull('finished_at');
    }

    /**
     * @return $this
     */
    public function markDone()
    {
        $this->finished_at = Carbon::now();
        $this->save();
        return $this;
    }

    /**
     * @return $this
     */
    public function markUndone()
    {
        $this->finished_at = null;
        $this->save();
        return $this;
    }
}
