<?php
namespace App\Services;

use Carbon\Carbon;

class ReportService
{
    public function generateTodoReportForUser($user, $hourRange = 1)
    {
        $startTime = Carbon::now()->subHours($hourRange);

        $startLog = $user->logs()->where('created_at', '<', $startTime)
                                 ->orderByDesc('created_at')
                                 ->first();

        $startCount = $startLog ? $startLog->undone_todos_count : 0;
        $result = [];
        $result[] = [$startTime->toDateTimeString(), $startCount];

        $logs = $user->logs()->where('created_at', '>=', $startTime)->get();

        if($logs->isEmpty()) {
            $result[] = [Carbon::now()->toDateTimeString(), $startCount];
        }

        foreach ($logs as $log) {
            $result[] = [$log->created_at->toDateTimeString(), $log->undone_todos_count];
        }

        return $result;
    }
}
