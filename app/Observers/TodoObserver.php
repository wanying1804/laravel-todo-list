<?php
namespace App\Observers;

use App\Models\Todo;
use Illuminate\Support\Facades\Auth;

/**
 * Class TodoObserver
 * @package App\Observers
 */
class TodoObserver
{
    /**
     * @param Todo $todo
     */
    public function created(Todo $todo)
    {
       $user = Auth::user();
       $user->logs()->firstOrCreate(
           [
               'log' => 'Todo ' . $todo->id . ' is created',
               'undone_todos_count' => $user->todos()->undones()->count()
           ]
       );
    }


    /**
     * @param Todo $todo
     */
    public function updated(Todo $todo)
    {
        if ($todo->isDirty('finished_at')) {
            $user = Auth::user();
            if ($todo->finished_at) {
                $user->logs()->firstOrCreate(
                    [
                        'log' => 'Todo ' . $todo->id . ' is done',
                        'undone_todos_count' => $user->todos()->undones()->count()
                    ]
                );
            } else {
                $user->logs()->firstOrCreate(
                    [
                        'log' => 'Todo ' . $todo->id . ' is undone',
                        'undone_todos_count' => $user->todos()->undones()->count()
                    ]
                );
            }
        }
    }

    /**
     * @param Todo $todo
     */
    public function deleted(Todo $todo) {
        $user = Auth::user();
        $user->logs()->firstOrCreate(
            [
                'log' => 'Todo ' . $todo->id . ' is deleted',
                'undone_todos_count' => $user->todos()->undones()->count()
            ]
        );
    }
}