import Vue          from 'vue';
import ElementUI    from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import locale       from 'element-ui/lib/locale/lang/en';

Vue.use(ElementUI, { locale });

Vue.component('todo-list', require('./components/TodoList.vue'));
Vue.component('todo-list-form', require('./components/TodoListForm.vue'));
Vue.component('todo-list-report', require('./components/TodoListReport.vue'));

new Vue({
    el: '#app',
});