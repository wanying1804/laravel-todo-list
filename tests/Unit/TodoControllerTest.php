<?php
namespace Agrigate\Invitations\Tests\Http\Controllers;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class TodoControllerTest extends TestCase {
    use DatabaseTransactions;

    const DUMMY_TASK = 'some task';


    public function setUp()
    {
        parent::setUp();
        
        $this->withoutMiddleware();
    }

    public function testGetTodos()
    {
        $user = $this->loginAsUser();

        $user->todos()->create([
            'task' => self::DUMMY_TASK
        ]);

        $this->get('/todo/get')->assertJsonCount(1);
    }

    public function testStoreTodo()
    {
        $user = $this->loginAsUser();
        $this->post('/todo/store', [
            'task' => 'some task'
        ])->assertJsonCount(1);

        $this->assertDatabaseHas('todos', [
            'user_id'  => $user->id,
            'task'     => self::DUMMY_TASK
        ]);

        $this->assertDatabaseHas('logs', [
            'user_id'  => $user->id,
            'undone_todos_count' => 1
        ]);
    }

    public function testDeleteTodo()
    {
        $user = $this->loginAsUser();
        $todo = $user->todos()->create([
            'task' => self::DUMMY_TASK
        ]);

        $this->post('/todo/delete/' . $todo->id);

        $this->assertSoftDeleted('todos', [
            'user_id'  => $user->id,
            'task'     => self::DUMMY_TASK
        ]);

        $this->assertDatabaseHas('logs', [
            'user_id'  => $user->id,
            'undone_todos_count' => 0
        ]);
    }
}