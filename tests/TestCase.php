<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * Create an user.
     *
     * @param array $attributes
     *
     * @return User
     */
    protected function createUser(array $attributes = [])
    {
        $user = factory(User::class)->create($attributes);

        return $user;
    }

    /**
     * Login the given user or create the first if none supplied.
     *
     * @param $user
     *
     * @return User
     */
    protected function loginAsUser($user = null)
    {
        if (!$user) {
            $user = $this->createUser();
        }

        $this->actingAs($user);

        return $user;
    }
}
