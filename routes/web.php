<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/todo/get', 'TodoController@getTodos');
Route::post('/todo/store', 'TodoController@storeTodo');
Route::post('/todo/done/{id}', 'TodoController@markTodoAsDone');
Route::post('/todo/undone/{id}', 'TodoController@markTodoAsUndone');
Route::post('/todo/delete/{id}', 'TodoController@deleteTodo');
Route::get('/todo/report.json', 'TodoController@getReport');
