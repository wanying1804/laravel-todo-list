# Todo-List
A simple todo list project built in Laravel and vuejs

## Setup
- Git clone the repo
- run `composer install`
- Setup .env file based on .env.example
- run `php artisan migrate`
- run `npm install`
- run `npm run dev`